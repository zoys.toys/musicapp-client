package com.example.mymusicappclient2;

import android.content.ContentProvider;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

//    RecyclerView recyclerView;
//    RecyclerViewAdapter adapter;
//    DatabaseReference mDatabase;
//    ProgressDialog progressDialog;
//    private List<Upload> uploads;

    private int load_time = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent home = new Intent(MainActivity.this,Register.class);

                startActivity(home);
                finish();
            }
        },load_time);
    }
//        recyclerView = findViewById(R.id.recyclerview_id);
//        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
//        progressDialog = new ProgressDialog(this);
//        uploads = new ArrayList<>();
//        progressDialog.setMessage("please wait ...");
//        progressDialog.show();
//        mDatabase = FirebaseDatabase.getInstance().getReference("uploads");
//        mDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                progressDialog.dismiss();
//                for (DataSnapshot postsnapshot : snapshot.getChildren()){
//                    Upload upload = postsnapshot.getValue(Upload.class);
//                    uploads.add(upload);
//                }
//                adapter = new RecyclerViewAdapter(getApplicationContext(),uploads);
//                recyclerView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//                progressDialog.dismiss();
//            }
//        });








}